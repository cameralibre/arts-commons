# Making Art Collaboratively, to Make Art Collaborative - a Collage Workshop.

First prototype tested July 2nd 2016 in Berlin, at Supermarkt's first [Arts & Commons](http://www.supermarkt-berlin.net/en/arts-commons/) event. Pictures & writeup on [cameralibre.cc](http://cameralibre.cc/arts-commons)

I'm currently developing a new version of this workshop which uses a photocopier to allow forking. See the proposal submitted for MozFest - _[Cut, Copy & Paste: Use (analog!) open source collaboration to make your own MozFest zine](https://github.com/MozillaFoundation/mozfest-program-2017/issues/472)_.

*This workshop was heavily influenced by two workshops I experienced at [OuiShare Fest 2016](http://2016.ouisharefest.com/): [Jacopo Romei](https://twitter.com/jacoporomei)'s [LEGO® SERIOUSPLAY®](http://www.lego.com/en-us/seriousplay/the-method) workshop and [Mercè Rua](https://twitter.com/mruaf)'s [collaborative leadership workshop](https://ouisharefestparis2016.sched.org/event/6hbv/how-can-we-create-a-more-successful-working-culture-through-collaborative-leadership?). Mercè knows A LOT more about this kind of thing than I do, so I'm very thankful that she could also contribute to the development of this prototype, by giving me feedback and suggestions from her background in facilitation, [user-centred design](http://www.holon.cat/) and improv theatre. Many thanks also to [Genevieve Parkes](https://github.com/genevievebelle) and [Judith Carnaby](http://www.judithcarnaby.com/) for their help in refining ideas and hunting out imagery!*

----

## Vision

I want to get artists excited about co-creating and customizing an artistic commons - I want us to be designing, writing stories, making illustrations and animations in a similar way to how people currently create Wikipedia and Free/LibreOpen Source Software. There are [people working on this](http://constantvzw.org), but not enough!

I'm starting to experiment too, in the field of [vector-based animation](https://gitlab.com/cameralibre/FragDenStaat-Animation), but for this workshop I wanted to keep barriers to entry low, and enthusiasm levels high, so it had to be an analogue process (there are few bigger buzzkills than rote-learning [git](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) commands). I set out to give participants an experience which mimics certain aspects of the open source process in a hands-on, analogue way: building upon the work of others, adding your individual contribution to a larger project, and inviting collaboration by documenting your work. One thing that is different with Open Source Collaboration to, say, collaborating with teammates in your office or studio, is that with open source you're often collaborating with *unknown others*. 
You can't possibly know in advance everybody who might find your project useful, who might have a good idea for it or take it further, so you should instead aim to document things for the widest possible audience that you can, to give people context, knowledge and permission to contribute to the project. This hugely opens up the number of potential directions your project could go in, and how far it can develop in those directions.

## Goals of the Workshop:

1. Use art-making to initiate a deep discussion on a particular topic, and uncover different perspectives and connections.
2. Give participants the experience of collectively creating, and collectively owning artworks 
3. Show participants that there is a wealth of raw creative material already available in the commons, which they can build upon.
4. Help them understand the relationship between good documentation and effective collaboration, and ground the practice of documentation as a key part of the creative process.


## Length of time needed: 
**40mins - 1 hour**, depending on how many teams & rounds you have.

## Participants: 
**5-30 participants** and a facilitator.  
No special professional background or artistic skills are required, though the workshop would be difficult without having a shared language. (We ran the prototype with 11 participants - some of the teams worked in German, but documented in English so that other teams could collaborate with them.)

## Materials needed:
- [Public Domain Images](https://gitlab.com/cameralibre/arts-commons/tree/master/Public%20Domain%20Images) 
- Plain paper for the canvas  
- Lined/Grid paper for documentation  
- Coloured paper  
- Scissors  
- Glue  
- Pens  
- A large table to make a mess on

## Optional warm-up activities
**5 min**

My particpants came to the workshop on-topic and engaged, shortly after another workshop on Arts & Commons, so we didn't have much need, nor extra time to do a warm-up. But in some cases it may be worth 'getting them in the mood' or introducing the techniques they will be using. For example:

### Yes, and...
You could get people ready to collaborate by getting them in improv theatre's *"[Yes, and...](https://en.wikipedia.org/wiki/Yes,_and...)"* mindset. 

Mercè did this at OuiShare Fest, asking us to pair up and then spend 1 minute planning a birthday party for our common friend, 'Julie'. However, every response we uttered had to start with the word *"No"*. Obviously the conversation is very difficult, frustrating, and doesn't get far at all.   
Next, we repeated the exercise, but started every response with *"Yes, but..."* which yielded slightly better results, but we still got nowhere, we just couldn't collaborate effectively. Finally we tried again, starting every response with *"Yes, and..."* 

Collaboration unlocked! we could not only plan Julie a party but we could think well outside the normal bounds of party planning:  
*"We could have it on a boat!* Yes, and Julie should water-ski behind it! Yes, and the band can sing sea shanties on the deck. Yes, and the cake should glow with phosphorescence! Yes, and it can be delivered by a friendly orca whale!"*

### Mini-collage workshop
Jacopo starts his LEGO® SERIOUSPLAY® workshops by giving all participants the same 9 pieces and asking them to "build a duck". It's an easy task, but it introduces the participants to the process in a hands-on way, and they realise a couple of things from it:  
Everyone's duck is different, despite having the same pieces, and once it is built, there is plenty you can say about your duck - what its story is, what kind of a personality it has... it's an easy way to get people used to the process before diving in to deeper topics. You could do something similar by breaking down the collage workshop to its bare essentials and doing a 3min miniature version of some kind.

## Introduction
**5 min**

Once your main table is scattered with public domain images, you can start by framing the topic that you will be making the collages about. Don't give a big lecture, the individual ideas should come from the participants, so just provide enough to get them in thinking in the same general area.  
For my workshop, I wanted us to think about **collaborative art-making**: to consider common perceptions such as the myth of the *'lone genius'* or the perception of group work as *'[design by committee](https://en.wikipedia.org/wiki/Design_by_committee)'* or at least something inherently difficult.  
I asked them to question *why* such perceptions or fears have emerged, and to think about their own perspectives on these issues. To think of specific times in their own life when they have tried to work with others on a creative goal: *"what did you enjoy, and what did you find difficult? what were the barriers to success? What processes or approaches help or hinder co-creation? What made you more or less likely to engage in such collaborative creative processes again?"*

I then explained that we will be making collages together. I find it useful to only give participants information about upcoming steps in the process *when they actually need it*, rather than explaining everything at the start. So I said nothing about documentation, passing works around, etc.


## Split the group
I had 11 participants, so I split them into four groups of 2 and one group of 3. With 5 groups in total, that would mean 5 rounds of co-creation. In hindsight I think that 4 groups / 4 rounds would have been better, but it still worked fine with 5.

## Round One 
**5 min**

The particpants have 2-3 minutes to discuss the topic with their partners, to find out what they think about the issue, and to share experiences. Then they have another two minutes to dig into the pile of images and try to find one or two images which can represent their ideas in some way. Try to use an example or get across the idea that these can (should) be very abstract representations. 

*"Obviously it is unlikely that collaboration broke down because one of your team members was a T-Rex, though that would be very understandable... but maybe the T-Rex can symbolize something in the collaboration process..? Or perhaps the T-Rex might represent one element in collaboration, and the Triceratops another?"*

## Documentation Round One
**2 min**

Hand out documentation paper and a pen to each team, and introduce the idea of documentation. 

*"Instead of sharing your thoughts with the whole group, you're going to write an explanation of why you chose these images and what they represent to you. You will be passing your images and this documentation on to the next group, who will build upon your ideas. It can also be though of as 'notes to self' or a recipe, or a to-do list"*  
The teams now have 1-2 min to write down some documentation.  
You can give them suggestions: *"what were the core ideas you wanted to express? why did you choose this particular image? what symbolism or meaning does it have for you? are there ideas which you discussed which you haven't included here, or you think should be better represented?"*

Once the teams have had time to write some documentation, ask each of them to pass their chosen images and their documentation around the table to the next team on their left. 

## Round Two
**4 min**

Now everyone has a new themes to work on, and some new images to play with. Gradually bring in the idea of *building on the work of the previous team* - ask the participants to read through the documentation and try to understand what the previous team was thinking, discuss the themes they chose and the images they picked to represent those themes. Emphasise that this documentation is setting the goals for them now - is not about carrying on exactly the same work that they did in Round One, their task is to continue the work of the previous team, to work on the topics they set and with the images they chose. 

Their task in Round Two is to discuss the topics in the documentation, consider how well the chosen images represent them, and think about what other elements could support these ideas, or what new aspects could be introduced. Each team gets an A3 sheet of paper as a 'canvas'. 
Select a couple more images to support or build upon the original idea, and find some kind of meaningful arrangement of the images together- which ones should be linked with others, which should be central, dominent, or less prominently placed? How does the meaning change when you put different elements together?

## Documentation Round Two
**2 min**

Once again, the teams explain what they did and why. What else did they try? what worked? What didn't? why did they choose this arrangement? 
Ask the teams to carefully pass on their artwork with the documentation to the next team.

## Round Three
**4 min**

The teams should have the hang of it by now. Add in another task - for my workshop I asked the participants to add in an element of some kind, cut from coloured paper. They can also consider what the colour might represent. This could be an abstract piece, it could be a representative or symbolic figure. I also encouraged them to cut something, to remove part of the arrangement made by the previous teams - it doesn't have to be a whole image, it can just be part of one - the idea here is to encourage them to take ownership of the piece. 

## Documentation Round Three
**2 min**

They can also give the following teams suggestions, for aspects which need work or ideas to try out.

## Round Four
**4 min**

Now the glue gets handed out, they can start making some more 'permanent' decisions.

## Documentation Round Four
**2 min**

As the next team will be presenting the finished work back to the rest of the group, the documentation here might also include instructions for presenting it.

## Round Five 
**4 min**

In our workshop we had another round post-gluing for making minor changes, getting an understanding of the work and presenting it back to the group. 

Next time I would combine Rounds 4 & 5, unless I had a specific and interesting activity to add to the process. Feedback from the group also suggested that they didn't feel so much ownership for the final piece as it was already glued down, they didn't have the ability to change very much - it felt like a token gesture.

## Presentation
**10 min**

In turn, each team presents "their" work back to the rest of the group - explaining the symbolism, what they were trying to do, why they made certain decisions (even if the decision was actually taken by a previous team, the current team's job is to represent the community behind the work - i.e. not to *take credit* from them, but rather to take responsibility for the piece - if an earlier team's decision can still be seen in the final piece, then it has been implicitly accepted and supported by those who followed them. 

Both to test this 'taking of responsibility' and to uncover a little more of the unspoken aspects on this 'creative collaboration' topic that we were exploring, I introduced another aspect borrowed from the LEGO SERIOUS PLAY workshop (do I really have to type it in caps with ® signs every time..? really?). 
The idea is to ask questions regarding the meaning of certain choices, certain connections between elements. Often the reaction might be to say *"oh no, there's no meaning to that, I just randomly put it there"* and this may well be true! But even so, encourage the participants to *find* a meaning anyway, and express it to the group - this can reveal deeper or more difficult-to-reach insights on the topic. Once you've shown the participants how it works, they will join in asking questions too.
For us it was fun, but I think more attention should be paid to bringing in and repeating this practice early on in order to be more successful and better reveal those 'deeper truths'.

## Reflection round
**5 min**

Ask the participants for their thoughts on the topic you were working on - was anything new or surprising revealed? What new insights do they have? How did they find the collaboration process? How do they feel about the artworks themselves? Do they feel more ownership or responsibility for particular works? why? Ask for feedback on the workshop process too! what worked well? what didn't?

## Context
Now that everyone had an understanding of this co-creative process, of building on others' work and documenting decisions, it's important to emphasise that really this is just a tiny taste of the open source process - two missing elements are Forking and Quality Control.

### Forking
Imagine if we could go back to any point in these artworks' creation, and create parallel versions in which we made different choices... Imagine if we could take elements from an early version of Collage 1 and include them in a late version of Collage 4, then tile that in a pattern and use it as a background for Collage 3... Imagine if we could explore a silly idea all the way to its conclusion, just to find out if it really was all that silly after all, and then we could revert all the way back again and work from an earlier, non-silly version... Well, all of these things are possible using methods of version control such as Git (or a wiki). And this is what gives the open source collaboration method so much power, especially when many minds can work with the same raw material. In our workshop, we had a linear process. Every time there was a fork in the road, we could only choose one path - we were only ever going to end up with 5 artworks. With 'forkable' digital works, we can choose many different parallel paths ourselves, but if we open source them, other people can take our ideas exponentially more paths and create fantastic, surprising things which we never could have imagined.

### Quality Control
Our workshop produced some valuable results, but I doubt many of our collages are going to win any art prizes. This is partly due to time restraints, but also due to a lack of quality control in the process. But if you can track and fork changes, then you can also maintain your own version of any particular artwork, and maintain as much (or as little) control over it as you like. In our workshop, once an artwork was out of your hands, you no longer had much control of it, save for your suggestions and instructions in the documentation. In the real world of open source, you have a number of methods for ensuring quality:

- **Clear goals & style guidelines.** By making it clear what you want to achieve and what kind of contributions you would like to see, you are already filtering out some contributions which may not be so useful to you, and encouraging potential contributors to work towards your goals.
- **Feedback and encouragement**. Maybe an initial contribution doesn't appear to be that helpful, or particularly high quality. With some empathy, patience and encouragement, as well as specific and constructive feedback, you may be able to convince the contributor to rework their contribution to fit your goals & style guidelines. This may take time initially but it is unlikely they'll need so much help next time.
- **Maintaining your own curated 'canon'**. There can be an 'anything goes' collection of all sorts of artworks of all sorts of qualities, and if they're all open source, you can simply pick and choose your selection of contributions and versions to make up your particular canon of works - it's fine for the works that you perceive to be lower quality to exist, but they won't have your name on them. Your reputation is tied only to the high quality selection that you personally manage.


# TO DO:
This was the first iteration of this workshop and it will need to be further refined. 

Please get in touch with suggestions - you can contact me via [email](mailto:sam@cameralibre.cc) , or [twitter](https://twitter.com/cameralibre), or if Git is more your bag, then a pull request will work too :)

## Preliminary ideas

- There is a lot more work that could be done on making each round more distinct and interesting. There should be a specific reason for the different activities rather than just *"um, ok, now let's add in some coloured paper"*
- I would like to make the 'Forking' and 'Quality Control' section a practical, hands-on part of the workshop in some way, so that the participants *experience it* rather than me just telling them. Ideas welcome!
- Mercè suggested incorporating a photocopier in the process to store each 'commit' and allow for participants to 'fork' artworks. (see the [current proposal](https://github.com/MozillaFoundation/mozfest-program-2017/issues/472) for updating this workshop)
- I'm all about the [circular economy](https://oscedays.org/open-source-circular-economy-mission-statement/), so I'm not so keen on producing large amounts of paper waste - any thoughts on how to make this process more 'circular' and reducing waste without losing the analogue, hands-on nature of the workshop?
